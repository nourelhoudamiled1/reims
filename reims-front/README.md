# reims-front

## Installation

### Install quasar cli

```bash
yarn global add @quasar/cli
```

If you are using Yarn, make sure that the Yarn global install location is in your PATH (you can usually skip this step):

```bash
# in ~/.bashrc or equivalent
export PATH="$(yarn global bin):$PATH"

# Under Windows, modify user's PATH environment variable
%LOCALAPPDATA%\yarn\bin
# Or to do this easily, enter the following code in the terminal:
setx path "%path%;%LocalAppData%\yarn\bin"
```

### Create the project

```bash
yarn create quasar
```

Follow onscreen instruction and select:

- What would you like to build?: `App with Quasar CLI, let's go!`
- Project folder: `reims-front`
- Pick Quasar version: `Quasar v2 (Vue 3 | latest and greatest)`
- Pick script type: `Javascript`
- Pick Quasar App CLI variant: `Quasar App CLI with Vite`
- Package name: `reims-front`
- Project product name: `reims`
- Project description: `reims`
- Author: `*****`
- Pick your CSS preprocessor: `Sass with SCSS syntax`
- Check the features needed for your project:
  - `ESLint `
  - `State Management (Pinia)`
  - `Axios `
  - `Vue-i18n`
- Pick an ESLint preset: `Prettier`
- Install project dependencies? (recommended): `Yes, use yarn`

### Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

If yarn is not in your path, you'll have to run

```bash
yarn quasar dev
```

### Lint the files

```bash
yarn lint
```

### Format the files

```bash
yarn format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).

### Docker

```bash
docker-compose up --build
```

### Set Axios path

Edit `baseURL` in src/boot/axios.js
