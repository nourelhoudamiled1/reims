<?php

use App\Http\Controllers\RuleController;
use App\Http\Controllers\TermController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//terms
Route::get('terms', [TermController::class, 'terms']);
Route::post('saveTerm', [TermController::class, 'saveTerm']);
Route::get('getTypes', [TermController::class, 'getTypes']);

//regle
Route::get('getOperators', [RuleController::class, 'getOperators']);
Route::get('rules', [RuleController::class, 'rules']);
Route::post('saveRule', [RuleController::class, 'saveRule']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
