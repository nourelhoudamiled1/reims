<?php

namespace App\Http\Resources;

use App\Models\Term;
use Illuminate\Http\Resources\Json\JsonResource;

class RuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $operatorMap = [
            "=" => "is",
            "<>" => "isNot",
            ">=" => "exactOrLaterThan",
            ">" => "laterThan",
            "<" => "earlierThan",
            "LIKE '$%'" => "startsWith",
            "NOT LIKE '$%'" => "doesNotStartWith",
            "LIKE '%$'" => "endsWith",
            "NOT LIKE '%$'" => "doesNotEndWith",
            "IS NULL" => "isEmpty",
            "IS NOT NULL" => "isNotEmpty",
        ];
        $conditionsDown = $this->conditions->where('type', 'down')->sortBy('order')->values();
        $conditionsUp = $this->conditions->where('type', 'up')->sortBy('order')->values();

        $conditionsDown = $conditionsDown->map(function ($condition) use ($operatorMap) {
            $term = Term::find($condition->term_id);

            return [
                'id' => $condition->id,
                'term' => [
                    'name' => $term->name,
                    'id' => $term->id,
                    'type' => $term->type,
                ],
                'order' => $condition->order,
                'operator' => [
                    'name' => $operatorMap[$condition->operator],
                    'operator' => $condition->operator,
                ],
                'value' => $condition->value,
                'block' => $condition->block,
                'type' => $condition->type,
                'rule_id' => $condition->rule_id,
                'created_at' => $condition->created_at,
                'updated_at' => $condition->updated_at,
            ];
        });
        $conditionsUp = $conditionsUp->map(function ($condition) use ($operatorMap) {
            $term = Term::find($condition->term_id);

            return [
                'id' => $condition->id,
                'term' => [
                    'name' => $term->name,
                    'id' => $term->id,
                    'type' => $term->type,
                ],
                'order' => $condition->order,
                'operator' => [
                    'name' => $operatorMap[$condition->operator],
                    'operator' => $condition->operator,
                ],
                'value' => $condition->value,
                'block' => $condition->block,
                'type' => $condition->type,
                'rule_id' => $condition->rule_id,
                'created_at' => $condition->created_at,
                'updated_at' => $condition->updated_at,
            ];
        });
        return [
            'id' => $this->id,
            'name' => $this->name,
            'conditions_down' => $conditionsDown,
            'conditions_up' => $conditionsUp,
        ];
    }
}
