<?php

namespace App\Http\Controllers;

use App\Http\Resources\TermResource;
use App\Models\Term;
use App\Repositories\TermRepository;
use App\Services\TermService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;


class TermController extends ApiController
{

    private TermRepository $termRepository;
    private TermService $termService;
    /**
     * @param TermRepository  $termRepository
     * @param TermService $termService
     */
    public function __construct(
        TermRepository $termRepository,
        TermService $termService
    ) {
        $this->termRepository = $termRepository;
        $this->termService = $termService;
    }

    public function terms()
    {
        $terms = $this->termRepository->terms();
        return $this->successResponse($terms);
    }


    public function saveTerm(Request $request)
    {
        $rules = [
            'type' => 'required|in:' . implode(',', ['integer', 'string', 'date']),
            'term_id' => 'nullable|exists:terms,id',
        ];
        if ($request->has('term_id') && $request->term_id) {
            $term = Term::find($request->term_id);
            $rules['name'] = 'required|unique:terms,name,' . $term->id . ',id';
        } else {
            $rules['name'] = ['required', Rule::unique('terms')];
        }
        $errors = $this->validateResponse($request->all(), $rules);
        if (!empty($errors)) {
            return $this->errorResponse($errors, 422);
        }
        $type = null;
        if ($request->has('type')) {
            $type = $request->type;
        }
        $termId = null;
        if ($request->has('term_id')) {
            $termId = $request->term_id;
        }
        $term = $this->termService->saveTerm(
            $termId,
            $request->name,
            $type,
        );
        return $this->successResponse(new TermResource($term));
    }

    public function getTypes()
    {
        $data = [
            ['id' => 'integer', 'name' => 'Number'],
            ['id' => 'string', 'name' => 'Text'],
            ['id' => 'date', 'name' => 'Date']
        ];
        return $this->successResponse($data);
    }
}
