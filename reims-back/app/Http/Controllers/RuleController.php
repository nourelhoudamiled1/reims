<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\RuleResource;
use App\Models\Rule as Rulee;
use App\Repositories\RuleRepository;
use App\Services\RuleService;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
class RuleController extends ApiController
{
    private RuleRepository $ruleRepository;
    private RuleService $ruleService;
    /**
     * @param RuleRepository  $ruleRepository
     * @param RuleService $ruleService
     */
    public function __construct(
        RuleRepository $ruleRepository,
        RuleService $ruleService
    ) {
        $this->ruleRepository = $ruleRepository;
        $this->ruleService = $ruleService;
    }
    /**
     * Get operators based on the given field type.
     * @param Request $request The HTTP request object containing the field type.
     * @return JsonResponse The JSON response containing the column operators.
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOperators(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'field_type' => 'required',
            ]
        );
        if ($validation->fails()) {
            return response()->json(
                $validation->errors(),
                Response::HTTP_BAD_REQUEST
            );
        }
        $data = [];
        $data["possible_values"] = [];
        if (($request->field_type == "date") || ($request->field_type == "integer") ) {
            $data["data"]  = [
                ["name" => "is", "operator" => "="], ["name" => "isNot", "operator" => "<>"],
                ["name" => "laterThan", "operator" => ">"],
                ["name" => "earlierThan", "operator" => "<"],
                ["name" => "exactOrLaterThan", "operator" => ">="],
                ["name" => "exactOrEarlierThan", "operator" => "<="],
                ["name" => "isEmpty", "operator" => "IS NULL"],
                ["name" => "isNotEmpty", "operator" => "IS NOT NULL"]
            ];
        } else {
            $data["data"] = [
                ["name" => "is", "operator" => "="], ["name" => "isNot", "operator" => "<>"],
                ["name" => "startsWith", "operator" => "LIKE '$%'"],
                ["name" => "doesNotStartWith", "operator" => "NOT LIKE '$%'"],
                ["name" => "endsWith", "operator" => "LIKE '%$'"],
                ["name" => "doesNotEndWith", "operator" => "NOT LIKE '%$'"],
                ["name" => "isEmpty", "operator" => "IS NULL"],
                ["name" => "isNotEmpty", "operator" => "IS NOT NULL"]
            ];
        }
        return $this->successResponse($data);

    }

    public function saveRule(Request $request)
    {
        $rules = [
            'name' => 'required',
            'rule_id' => 'nullable|exists:rules,id',
            'conditions_up' => 'nullable|array',
            'conditions_up.*.order' => 'nullable',
            'conditions_up.*.block' => 'nullable',
            'conditions_up.*.operator' => 'nullable',
            'conditions_up.*.value' => 'nullable',
            'conditions_up.*.term_id' => 'nullable',

            'conditions_down' => 'nullable|array',
            'conditions_down.*.order' => 'nullable',
            'conditions_down.*.block' => 'nullable',
            'conditions_down.*.operator' => 'nullable',
            'conditions_down.*.value' => 'nullable',
            'conditions_down.*.term_id' => 'nullable',
        ];
        if ($request->has('rule_id') && $request->rule_id) {
            $rule = Rulee::find($request->rule_id);
            Log::info($rule);
            $rules['name'] = ['required', Rule::unique('rules')->ignore($rule->id)];
        } else {
            $rules['name'] = ['required', Rule::unique('rules')];
        }
        $errors = $this->validateResponse($request->all(), $rules);
        if (!empty($errors)) {
            return $this->errorResponse($errors, 422);
        }

        $conditions_down = null;
        if ($request->has('conditions_down')) {
            $conditions_down = $request->conditions_down;
        }
        $conditions_up = null;
        if ($request->has('conditions_up')) {
            $conditions_up = $request->conditions_up;
        }
        // dd($conditions_up);
        $ruleId = null;
        if ($request->has('rule_id')) {
            $ruleId = $request->rule_id;
        }
        $rule = $this->ruleService->saveRule(
            $ruleId,
            $request->name,
            $conditions_up,
            $conditions_down
        );
        return $this->successResponse(new RuleResource($rule));
    }

    public function rules()
    {
        $rules = $this->ruleRepository->rules();
        return $this->successResponse($rules);
    }
}
