<?php

namespace App\Repositories;

use App\Models\Rule;
use App\Models\Term;

/**
 * Class RuleRepository
 * @package App\Repository
 */
class RuleRepository
{
    /**
     * Get the terms.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function rules()
    {
        /** @var \Illuminate\Database\Eloquent\Builder $data */
        $data = Rule::select(['rules.name', 'rules.id'])
            ->orderByDesc('rules.id')
            ->get()->map(function ($rule) {
                $conditionsDown = $rule->conditions->where('type', 'down')->sortBy('order')->values();
                $conditionsUp = $rule->conditions->where('type', 'up')->sortBy('order')->values();
                $operatorMap = [
                    "=" => "is",
                    "<>" => "isNot",
                    ">=" => "exactOrLaterThan",
                    ">" => "laterThan",
                    "<" => "earlierThan",
                    "LIKE '$%'" => "startsWith",
                    "NOT LIKE '$%'" => "doesNotStartWith",
                    "LIKE '%$'" => "endsWith",
                    "NOT LIKE '%$'" => "doesNotEndWith",
                    "IS NULL" => "isEmpty",
                    "IS NOT NULL" => "isNotEmpty",
                ];
                $conditionsDown = $conditionsDown->map(function ($condition) use ($operatorMap) {
                    $term = Term::find($condition->term_id);
                    return [
                        'id' => $condition->id,
                        'term' => [
                            'name' => $term->name,
                            'id' => $term->id,
                            'type' => $term->type,
                        ],
                        'order' => $condition->order,
                        'operator' => [
                            'name' => $operatorMap[$condition->operator],
                            'operator' => $condition->operator,
                        ],
                        'value' => $condition->value,
                        'block' => $condition->block,
                        'type' => $condition->type,
                        'rule_id' => $condition->rule_id,
                        'created_at' => $condition->created_at,
                        'updated_at' => $condition->updated_at,
                    ];
                });
                $conditionsUp = $conditionsUp->map(function ($condition) use ($operatorMap) {
                    $term = Term::find($condition->term_id);

                    return [
                        'id' => $condition->id,
                        'term' => [
                            'name' => $term->name,
                            'id' => $term->id,
                            'type' => $term->type,
                        ],
                        'order' => $condition->order,
                        'operator' => [
                            'name' => $operatorMap[$condition->operator],
                            'operator' => $condition->operator,
                        ],
                        'value' => $condition->value,
                        'block' => $condition->block,
                        'type' => $condition->type,
                        'rule_id' => $condition->rule_id,
                        'created_at' => $condition->created_at,
                        'updated_at' => $condition->updated_at,
                    ];
                });
                $conditionsDownString = $conditionsDown->reduce(function ($carry, $condition) {
                    $conditionString = "{$condition['term']['name']} {$condition['operator']['operator']} {$condition['value']}";
                    if ($condition['block'] === 'and') {
                        return $carry ? "$carry ET $conditionString" : $conditionString;
                    } else {
                        return $carry ? "$carry OU $conditionString" : $conditionString;
                    }
                }, '');

                $conditionsUpString = $conditionsUp->reduce(function ($carry, $condition) {
                    $conditionString = "{$condition['term']['name']} {$condition['operator']['operator']} {$condition['value']}";
                    if ($condition['block'] === 'and') {
                        return $carry ? "$carry ET $conditionString" : $conditionString;
                    } else {
                        return $carry ? "$carry OU $conditionString" : $conditionString;
                    }
                }, '');

                $conditionsString = ($conditionsUpString && $conditionsDownString) ? "($conditionsUpString) OU ($conditionsDownString)" : ($conditionsUpString ?: $conditionsDownString);
                $conditionsArbre = '';
                if ($conditionsUpString && $conditionsDownString) {
                    $conditionsArbre = [
                        'type' => 'or',
                        'conditions' => [
                            ['type' => 'group', 'value' => $conditionsUpString],
                            ['type' => 'group', 'value' => $conditionsDownString],
                        ],
                    ];
                } elseif ($conditionsUpString) {
                    $conditionsArbre = [
                        'type' => 'group',
                        'value' => $conditionsUpString,
                    ];
                } elseif ($conditionsDownString) {
                    $conditionsArbre = [
                        'type' => 'or',
                        'conditions' => [
                            ['type' => 'group', 'value' => $conditionsDownString],
                        ],
                    ];
                }
                return [
                    'id' => $rule->id,
                    'name' => $rule->name,
                    'conditions_down' => $conditionsDown,
                    'conditions_up' => $conditionsUp,
                    'conditions_string' => $conditionsString,
                    'conditions_arbre' => $conditionsArbre,

                ];
            });

        return $data;
    }
}
