<?php

namespace App\Repositories;

use App\Models\Term;

/**
 * Class TermRepository
 * @package App\Repository
 */
class TermRepository
{
    /**
     * Get the terms.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function terms()
    {
        /** @var \Illuminate\Database\Eloquent\Builder $data */
        $data = Term::select([
            'terms.name',
            'terms.id',
            'terms.type'
        ])->orderByDesc('terms.id')
            ->get();

        return $data;
    }
}
