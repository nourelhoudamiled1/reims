<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Term extends Model
{
    protected $table = 'terms';

    protected $fillable = [
        'name',
        'type'
    ];


}
