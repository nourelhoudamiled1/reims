<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{

    protected $table = "conditions";

    protected $fillable = ["term_id", "order", "operator", "value", "block", "type", "rule_id"];

    public function rule()
    {
        return $this->belongsTo(Rule::class, 'rule_id');
    }

}
