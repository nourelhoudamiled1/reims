<?php

namespace App\Services;

use App\Models\Condition;
use App\Models\Rule;
use Illuminate\Support\Facades\Log;

/**
 * Class ConditionService
 * @package App\Services
 */
class ConditionService
{


    public function saveCondition(
        $rule,
        $conditions_down,
        $conditions_up

    ) {
        Condition::where('rule_id', $rule->id)->delete();
        if($conditions_down){
            foreach($conditions_down as $down){
                $this->newCondition($down, $rule->id, 'down');
            }
        }
        if ($conditions_up) {
            foreach ($conditions_up as $up) {
                $this->newCondition($up,  $rule->id, 'up');
            }
        }

    }
    public function newCondition($condition, $rule_id, $type){
        $conditionNew = new Condition();
        Log::info($condition);
        $conditionNew->term_id = isset($condition['term_id']) ? $condition['term_id'] : null;
        $conditionNew->order = isset($condition['order']) ? $condition['order'] : null;
        $conditionNew->operator = isset($condition['operator']) ? $condition['operator'] : null;
        $conditionNew->value = isset($condition['value']) ? $condition['value'] : null;
        $conditionNew->block = isset($condition['block']) ? $condition['block'] : null;
        $conditionNew->type = $type;
        $conditionNew->rule_id = $rule_id;
        $conditionNew->save();
    }
}
