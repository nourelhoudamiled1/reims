<?php

namespace App\Services;

use App\Models\Rule;
use Illuminate\Support\Facades\Log;

/**
 * Class RuleService
 * @package App\Services
 */
class RuleService
{
    private ConditionService $conditionService;
    /**
     * @param ConditionService $conditionService
     */
    public function __construct(
        ConditionService $conditionService
    ) {
        $this->conditionService = $conditionService;
    }
    /**
     * save Rule.
     * @param  int|null  $ruleId  The ID of the Rule to update. Defaults to null, meaning create a new Rule.
     * @param  string  $name  The name of the Rule.
     * @param  array  $conditions_up  The conditions_up of the Rule.
     * @param  array  $conditions_down  The conditions_down of the Rule.
     * @return Rule The saved Rule.
     */
    public function saveRule(
        $ruleId = null,
        $name,
        $conditions_up,
        $conditions_down
    ): Rule {
        $rule = Rule::find($ruleId);
        if (!$ruleId) { //create Rule
            $ruleNew = new Rule();
            $ruleNew->name = $name;
            $ruleNew->save();
            $rule = $ruleNew;
        } else { // update rule
            $rule->name = $name;
            $rule->save();
        }


        $this->conditionService->saveCondition($rule, $conditions_down, $conditions_up);

        return $rule;
    }
}
