<?php

namespace App\Services;

use App\Models\Term;
use Illuminate\Support\Facades\Log;

/**
 * Class TermService
 * @package App\Services
 */
class TermService
{

    /**
     * save Term.
     * @param  int|null  $termId  The ID of the Term to update. Defaults to null, meaning create a new Term.
     * @param  string  $name  The name of the Term.
     * @param  string  $type  The type of the Term.
     * @return Term The saved Term.
     */
    public function saveTerm(
        $termId = null,
        $name,
        $type
    ): Term {
        $term = Term::find($termId);
        if (!$termId) { //create Term
            $termNew = new Term();
            $termNew->name = $name;
            $termNew->type = $type;
            $termNew->save();
            $term = $termNew;
        } else { // update term
            $term->name = $name;
            $term->type = $type;
            $term->save();
        }
        return $term;
    }
}
