<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('term_id');
            $table->string('order')->nullable();
            $table->string('operator')->nullable();
            $table->text('value')->nullable();
            $table->string('block')->nullable();
            $table->string('type')->nullable();
            $table->unsignedBigInteger('rule_id')
            ->references('id')->on('terms')
                ->onDelete('cascade');
            $table->foreign('rule_id')
            ->references('id')->on('rules')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conditions');
    }
};
